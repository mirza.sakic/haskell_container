FROM ubuntu:18.04

#Set Debian frontend to noninteractive mode
#----------------------------------------------------------------------------------------------
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

# Install tzdata and configure timezone
#----------------------------------------------------------------------------------------------
RUN apt update && apt -y install tzdata && \
    ln -s -f /usr/share/zoneinfo/Europe/Sarajevo /etc/localtime && \
    dpkg-reconfigure --frontend noninteractive tzdata && \
    apt -y autoremove && apt -y autoclean

RUN apt update && apt -y upgrade && \
    apt -y install \
    sudo \
    pkg-config \
    software-properties-common \
    htop \
    vim \
    build-essential \
    net-tools \
    debconf-utils \
    apt-utils \
    locales \
    wget \
    xclip \
    unzip \
    zlib1g-dev \
    git \
    curl \
    silversearcher-ag \
    python-pip \
    python3-pip \
    zsh \
    man \
    rsync \
    gosu \
    ca-certificates \
    libicu-dev \ 
    libtinfo-dev \
    libgmp-dev \
    nodejs && \
    apt -y autoremove && apt -y autoclean

RUN python -m pip install --upgrade neovim

RUN python3 -m pip install --upgrade neovim

RUN yes | adduser --uid 2000 user1 && \
    chown -R user1:user1 /home/user1 && \ 
    locale-gen en_US.UTF-8

ENV HOME="/home/user1" \
    LANG="en_US.UTF-8" \
    LANGUAGE="en_US:en" \
    LC_ALL="en_US.UTF-8" \
    PATH="${PATH}:/opt/bin:/opt/stack-local-bin" \
    STACK_ROOT="/opt/bin"

# oh-my-zsh
#----------------------------------------------------------------------------------------------
RUN	wget -qO- https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O - | bash  

# Powerline theme
#----------------------------------------------------------------------------------------------
RUN	git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k 

# ZSH goodies
#----------------------------------------------------------------------------------------------
RUN git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions && \
    git clone https://github.com/zsh-users/zsh-completions ~/.oh-my-zsh/custom/plugins/zsh-completions && \ 
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting 

# Install fzf
#----------------------------------------------------------------------------------------------
RUN git clone --depth 1 https://github.com/junegunn/fzf.git ${HOME}/.fzf && \
    ${HOME}/.fzf/install

# Prepare /opt/bin
#----------------------------------------------------------------------------------------------
RUN mkdir /opt/bin

# Install cabal
#----------------------------------------------------------------------------------------------
RUN cd /opt/bin && \
    wget https://downloads.haskell.org/~cabal/cabal-install-3.2.0.0/cabal-install-3.2.0.0-x86_64-unknown-linux.tar.xz && \
    tar xvf cabal-install-3.2.0.0-x86_64-unknown-linux.tar.xz && \
    rm -rf cabal-install-3.2.0.0-x86_64-unknown-linux.tar.xz

RUN cd /opt/bin && \
    wget https://github.com/haskell/haskell-language-server/releases/download/0.5.1/haskell-language-server-Linux-8.8.4.gz && \
    gunzip haskell-language-server-Linux-8.8.4.gz && \
    rm -rf haskell-language-server-Linux-8.8.4.gz && \
    chmod +x /opt/bin/haskell-language-server-Linux-8.8.4 && \
    ln -s /opt/bin/haskell-language-server-Linux-8.8.4 /opt/bin/haskell-language-server


RUN cd ${HOME} && \
    mkdir .config && \
    cd .config && \
    mkdir nvim_config && \
    cd nvim_config && \
    mkdir coc 

ADD nvim ${HOME}/.config/nvim_config/nvim

RUN cd ${HOME}/.config/nvim_config/nvim && \
    mkdir ${HOME}/.local && \
    mv nvim04 ${HOME}/.local/. && \
    cd ${HOME} && \
    PATH=${HOME}/.local/nvim04/bin/:${PATH} XDG_CONFIG_HOME=${HOME}/.config/nvim_config nvim --headless +PlugInstall +qall

COPY "config/.bashrc" "${HOME}/.bashrc"
COPY "config/.zshrc" "${HOME}/.zshrc"
COPY "config/entrypoint.sh" "/usr/local/bin/entrypoint.sh"

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]