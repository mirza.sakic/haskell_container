#!/bin/bash -e


  #Build user environment
  #-------------------------------------------------------------------------------
TEST_VAR="${USER_ID:=dummy}"
if [ $TEST_VAR == "dummy" ]; then
  exec gosu root "$@"
else
  groupadd -g $GROUP_ID $GROUP_NAME
  useradd -s /bin/bash -u $USER_ID -g $GROUP_ID  -M -N -d /home/$USER_NAME $USER_NAME
  #sudo -H -u $USER_NAME bash -c "sleep 5 && ~/update_distcc_hosts.sh $distcc_port 10.1.0.0/16 &"
  echo "$USER_NAME ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
  if [ ! -d "/home/$USER_NAME" ]; then
     mkdir -p /home/$USER_NAME
  fi

  #Force overwrite all files from container to the host volume
  echo "Syncing home dir..."
  if [ ! -f "/home/$USER_NAME/.do_not_overwrite_home_haskell" ]; then
    rsync -a /home/user1/ /home/$USER_NAME/
    chmod -R go=u,go-w /home/$USER_NAME/
    touch /home/$USER_NAME/.do_not_overwrite_home_haskell
  fi
  chown -R $USER_NAME.$GROUP_ID /home/$USER_NAME/

  #Change home path in nvim settings
  #sed -i "s#\"[^:]*\.ccls-cache.*\"#\"/home/$USER_NAME/.ccls-cache\"#g" /home/$USER_NAME/.config/nvim_config/nvim/coc-settings.json 

  #Added path to the poetry bin
  PATH="${PATH}:/home/$USER_NAME/.poetry/bin"

  echo "Starting with user(UID) : $USER_NAME($USER_ID)"
  exec gosu $USER_NAME "$@"
fi

