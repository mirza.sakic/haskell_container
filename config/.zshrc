# If you come from bash you might have to change your $PATH.
 #export PATH=$HOME/bin:/usr/local/bin:$PATH
 export TERM="xterm-256color"
 #
# # Path to your oh-my-zsh installation.
 export ZSH=$HOME/.oh-my-zsh
#
# # Set name of the theme to load --- if set to "random", it will
# # load a random theme each time oh-my-zsh is loaded, in which case,
# # to know which specific one was loaded, run: echo $RANDOM_THEME
# # See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
 ZSH_THEME="powerlevel9k/powerlevel9k"
#
# # Set list of themes to pick from when loading at random
# # Setting this variable when ZSH_THEME=random will cause zsh to load
# # a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# # If set to an empty array, this variable will have no effect.
# # ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )
#
# # Uncomment the following line to use case-sensitive completion.
# # CASE_SENSITIVE="true"
#
# # Uncomment the following line to use hyphen-insensitive completion.
# # Case-sensitive completion must be off. _ and - will be interchangeable.
# # HYPHEN_INSENSITIVE="true"
#
# # Uncomment the following line to disable bi-weekly auto-update checks.
# # DISABLE_AUTO_UPDATE="true"
#
# # Uncomment the following line to automatically update without prompting.
# # DISABLE_UPDATE_PROMPT="true"
#
# # Uncomment the following line to change how often to auto-update (in days).
# # export UPDATE_ZSH_DAYS=13
#
# # Uncomment the following line if pasting URLs and other text is messed up.
# # DISABLE_MAGIC_FUNCTIONS=true
#
# # Uncomment the following line to disable colors in ls.
# # DISABLE_LS_COLORS="true"
#
# # Uncomment the following line to disable auto-setting terminal title.
# # DISABLE_AUTO_TITLE="true"
#
# # Uncomment the following line to enable command auto-correction.
# # ENABLE_CORRECTION="true"
#
# # Uncomment the following line to display red dots whilst waiting for completion.
# # COMPLETION_WAITING_DOTS="true"
#
# # Uncomment the following line if you want to disable marking untracked files
# # under VCS as dirty. This makes repository status check for large repositories
# # much, much faster.
# # DISABLE_UNTRACKED_FILES_DIRTY="true"
#
# # Uncomment the following line if you want to change the command execution time
# # stamp shown in the history command output.
# # You can set one of the optional three formats:
# # "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# # or set a custom format using the strftime function format specifications,
# # see 'man strftime' for details.
# # HIST_STAMPS="mm/dd/yyyy"
#
# # Would you like to use another custom folder than $ZSH/custom?
# # ZSH_CUSTOM=/path/to/new-custom-folder
#
# # Which plugins would you like to load?
# # Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# # Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# # Example format: plugins=(rails git textmate ruby lighthouse)
# # Add wisely, as too many plugins slow down shell startup.
 plugins=(
 git
 pip
 python
 vscode
 common-aliases
 zsh-autosuggestions
 zsh-completions
 zsh-syntax-highlighting
 )

#For poetry
 fpath+=${HOME}/.zfunc
#
 source $ZSH/oh-my-zsh.sh
#
# # User configuration
#
# # export MANPATH="/usr/local/man:$MANPATH"
#
# # You may need to manually set your language environment
# # export LANG=en_US.UTF-8
#
# # Preferred editor for local and remote sessions
# # if [[ -n $SSH_CONNECTION ]]; then
# #   export EDITOR='vim'
# # else
# #   export EDITOR='mvim'
# # fi
#
# # Compilation flags
# # export ARCHFLAGS="-arch x86_64"
#
# # Set personal aliases, overriding those provided by oh-my-zsh libs,
# # plugins, and themes. Aliases can be placed here, though oh-my-zsh
# # users are encouraged to define aliases within the ZSH_CUSTOM folder.
# # For a full list of active aliases, run `alias`.
# #
# # Example aliases
# # alias zshconfig="mate ~/.zshrc"
# # alias ohmyzsh="mate ~/.oh-my-zsh"

#PATH=$(cat ~/.user_path)  #For .fzf

export LDFLAGS=-L/usr/local//lib
export CFLAGS=-I/usr/local/include
export CXXFLAGS=-I/usr/local/include

export CPATH=/usr/local/include
export LIBRARY_PATH=/usr/local/lib
export LD_LIBRARY_PATH=/usr/local/lib

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
[ -f ~/start_ssh_agent.sh ] && source ~/start_ssh_agent.sh
export FZF_DEFAULT_COMMAND='find . ! \( -name "*.o" -o -name "*.wrap" \) -not -path "./build/*" -not -path "*/.ccls-cache/*" -not -path "*subprojects/*.gitignore" -not -path "*subprojects/*subprojects*" -not -path "*.git/*" -not -path "*.vscode/*" -type f'
export FZF_DEFAULT_OPTS='--tac --tiebreak=end'

POWERLEVEL9K_CUSTOM_CONTAINER_NAME="echo \<H-DEV\>"
POWERLEVEL9K_CUSTOM_CONTAINER_NAME_BACKGROUND="black"
POWERLEVEL9K_CUSTOM_CONTAINER_NAME_FOREGROUND="green"
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(custom_container_name user dir vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status root_indicator background_jobs load history time)

POWERLEVEL9K_SHORTEN_DIR_LENGTH=1
POWERLEVEL9K_SHORTEN_STRATEGY=truncate_to_last

#[[ -s /home/user1/.autojump/etc/profile.d/autojump.sh ]] && source /home/user1/.autojump/etc/profile.d/autojump.sh
#autoload -U compinit && compinit -u

# _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Set e1 for the default editor
#git config --global core.editor e1

alias neo='XDG_CONFIG_HOME=${HOME}/.config/nvim_config nvim'

sudo chown $USER_NAME:$USER_NAME -R /opt/bin
export USER_HOME=/mnt/$HOME/
export PATH=${HOME}/.local/nvim04/bin:$USER_HOME/.haskell_container/ghc/bin/:$PATH
export CABAL_DIR=$USER_HOME/.haskell_container/cabal

if [ ! -d $USER_HOME/.haskell_container/ghc ]; then
    echo ""
    echo ""
    echo "Setting up The Glorious Glasgow Haskell Compiler!"
    echo ""
    echo ""
    mkdir -p $USER_HOME/.haskell_container
    mkdir $USER_HOME/.haskell_container/ghc
    cd $_
    wget https://downloads.haskell.org/\~ghc/8.8.4/ghc-8.8.4-x86_64-deb9-linux.tar.xz
    tar xf ghc-8.8.4-x86_64-deb9-linux.tar.xz
    cd ghc-8.8.4
    ./configure --prefix=$USER_HOME/.haskell_container/ghc
    make install
    rm -rf ghc-8.8.4-x86_64-deb9-linux.tar.xz
    cd
fi

if [ ! -d $USER_HOME/.haskell_container/cabal ]; then
    echo ""
    echo ""
    echo "Setting up cabal!"
    echo ""
    echo ""
    mkdir -p $USER_HOME/.haskell_container
    mkdir $USER_HOME/.haskell_container/cabal
    cabal update
    cd
else
    echo "Updating cabal..."
    cabal update
fi